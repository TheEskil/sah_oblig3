package oblig3.hine.no;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Oblig1 {

    public static void main(String[] args) {
        List<Kjoretoy> kjoretoyliste = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        boolean b = true;

        System.out.println("Meny");
        System.out.println("A - avslutt");
        System.out.println("L - legg til et kjøretøy");
        System.out.println("S - skriv ut kjøretøy");
        System.out.println("E - endre info om kjøretøy");
        System.out.println("R - søke på registreringsnummer");
        System.out.println("T - søke på type kjøretøy");

        while (b == true) {
            System.out.print("Ta et valg: ");
            char valg = scanner.nextLine().charAt(0);

            switch (valg) {
                case 'A':
                case 'a':
                    System.out.println("Avslutter programmet.");
                    b = false;
                    break;

                case 'L':
                case 'l':
                    System.out.print("Tast 1 for tohjuling.");
                    System.out.print("Tast 2 for firhjuling.");
                    System.out.print("Tast 3 for flerhjuling.");
                    int kjoretoyvalg = Integer.parseInt(scanner.nextLine());

                    System.out.print("Skriv inn regnr: ");
                    String regNr = scanner.nextLine();
                    System.out.print("Skriv inn antall passasjerer: ");
                    int antPass = Integer.parseInt(scanner.nextLine());
                    System.out.print("Skriv inn motorstr: ");
                    int motorStr = Integer.parseInt(scanner.nextLine());
                    System.out.print("Skriv inn maks hastighet: ");
                    int hastighet = Integer.parseInt(scanner.nextLine());
                    System.out.print("Skriv inn tankvolum: ");
                    int tankVol = Integer.parseInt(scanner.nextLine());
                    System.out.print("Skriv inn forbruk pr mil: ");
                    double forbrukPrMil = Double.parseDouble(scanner.nextLine());
                    System.out.print("Skriv inn antall hjul: ");
                    int antHjul = Integer.parseInt(scanner.nextLine());

                    int antDor;
                    Kjoretoy kjoretoy;

                    switch (kjoretoyvalg) {
                        case 1:


                            kjoretoy = new Tohjuling("tohjuling", antHjul, regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil);
                            kjoretoyliste.add(kjoretoy);

                            break;

                        case 2:

                            System.out.print("Skriv inn antall dører: ");
                            antDor = Integer.parseInt(scanner.nextLine());


                            kjoretoy = new Firhjuling(antDor, "firhjuling", antHjul, regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil);
                            kjoretoyliste.add(kjoretoy);

                            break;

                        case 3:

                            System.out.print("Skriv inn antall dører: ");
                            antDor = Integer.parseInt(scanner.nextLine());

                            kjoretoy = new Flerhjuling(antDor, "flerhjuling", antHjul, regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil);
                            kjoretoyliste.add(kjoretoy);

                            break;
                    }

                    System.out.println("Nytt kjøretøy ble opprettet.");
                    break;

                case 'S':
                case 's':
                    System.out.println(kjoretoyliste);
                    break;

                case 'E':
                case 'e':

                    int i = 0;
                    for (Kjoretoy k : kjoretoyliste) {
                        System.out.println("Index: " + i + ", " + k);
                        i++;
                    }

                    System.out.println("Hvilken indeks vil du endre på?");
                    int valgtKjoretoy = Integer.parseInt(scanner.nextLine());

                    System.out.println("Hva vil du endre på?");
                    System.out.println("Tast 1 for å endre regnr.");
                    System.out.println("Tast 2 for å endre antall passasjerer.");
                    System.out.println("Tast 3 for å endre motorstr.");
                    System.out.println("Tast 4 for å endre maks hastighet.");
                    System.out.println("Tast 5 for å endre tankvolum.");
                    int valgtEndring = Integer.parseInt(scanner.nextLine());

                    switch (valgtEndring) {
                        case 1:
                            System.out.println("Tast inn nytt regnr: ");
                            String nyttRegnr = scanner.nextLine();
                            kjoretoyliste.get(valgtKjoretoy).setRegNr(nyttRegnr);
                            System.out.println("Nytt regnr. er registrert.");
                            break;

                        case 2:
                            System.out.println("Tast inn nytt antall passasjerer: ");
                            int nyantPass = Integer.parseInt(scanner.nextLine());
                            kjoretoyliste.get(valgtKjoretoy).setAntPass(nyantPass);
                            System.out.println("Nytt antall passasjerer er registrert.");
                            break;

                        case 3:
                            System.out.println("Tast inn ny motorstr: ");
                            int nyMotorstr = Integer.parseInt(scanner.nextLine());
                            kjoretoyliste.get(valgtKjoretoy).setMotorStr(nyMotorstr);
                            System.out.println("Ny motorstr. er registrert.");
                            break;

                        case 4:
                            System.out.println("Tast inn ny maks hastighet: ");
                            int nyHastighet = Integer.parseInt(scanner.nextLine());
                            kjoretoyliste.get(valgtKjoretoy).setHastighet(nyHastighet);
                            System.out.println("Ny maks hastighet er registrert.");
                            break;

                        case 5:
                            System.out.println("Tast inn nytt tankvolum: ");
                            int nyttTankvol = Integer.parseInt(scanner.nextLine());
                            kjoretoyliste.get(valgtKjoretoy).setTankVol(nyttTankvol);
                            System.out.println("Nytt tankvolum er registrert.");
                            break;

                    }

                    break;

                case 'R':
                case 'r':
                    System.out.println("Skriv inn regnr. du vil søke på? ");
                    String sokRegnr = scanner.nextLine();
                    for (Kjoretoy k : kjoretoyliste) {
                        if (k.getRegNr().equals(sokRegnr)) {
                            System.out.println(k);
                        }
                    }

                    break;

                case 'T':
                case 't':
                    System.out.println("Hvilket kjøretøy vil du søke på? ");
                    System.out.println("Tast 1 for tohjulinger.");
                    System.out.println("Tast 2 for firhjulinger.");
                    System.out.println("Tast 3 for flerhjulinger.");
                    int valgtType = Integer.parseInt(scanner.nextLine());

                    switch (valgtType) {
                        case 1:
                            for (Kjoretoy k : kjoretoyliste) {
                                if (k instanceof Tohjuling) {
                                    System.out.println(k);

                                }
                            }
                            break;

                        case 2:
                            for (Kjoretoy k : kjoretoyliste) {
                                if (k instanceof Firhjuling) {
                                    System.out.println(k);
                                }

                            }
                            break;

                        case 3:
                            for (Kjoretoy k : kjoretoyliste) {
                                if (k instanceof Flerhjuling) {
                                    System.out.println(k);
                                }
                            }
                            break;

                    }

            }

        }
    }
}
