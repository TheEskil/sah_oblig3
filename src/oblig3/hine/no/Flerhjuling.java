/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oblig3.hine.no;


public class Flerhjuling extends KjoretoyMedDorer {

    public Flerhjuling(int antDor, String kjoretoyType, int antHjul, String regNr, int motorStr, int antPass, int hastighet, int tankVol, double forbrukPrMil) {
        super(antDor, kjoretoyType, antHjul, regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil);
    }

    @Override
    public String toString() {
        return "Flerhjuling{" + super.toString() + '}';
    }


}
