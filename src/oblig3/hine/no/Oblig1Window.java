package oblig3.hine.no;


import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;


public class Oblig1Window extends JFrame implements ActionListener, ListSelectionListener, ItemListener {

    private static final long serialVersionUID = 1L;

    private final JPanel contentPane;

    private List<Kjoretoy> kjoretoyer = new ArrayList<>();    // ArrayList for å ta vare på kjøretøy
    private static String kjoretoyFil = "kjoretoyListe.data";

    private DatabaseManager databaseManager = new DatabaseManager();

    public List<Kjoretoy> getkjoretoyer() {
        return this.kjoretoyer;
    }

    public void setkjoretoyer(List<Kjoretoy> kjoretoyer) {
        this.kjoretoyer = kjoretoyer;
    }


    private final JTextField regNr;
    private final JTextField motorStr;
    private final JTextField hastighet;
    private final JTextField tankVol;
    private final JTextField forbrukPrMil;

    private final JList<Kjoretoy> kjoretoyListe;

    private final JComboBox<String> kjoretoyType;

    private final JLabel errorLabel;
    private final JLabel lblAntDor;
    private final JLabel lblForbrukPrMil;
    private final JLabel lblDistansePrTank;
    private final JLabel lblValueDistansePrTank;

    private final JSpinner dorSpinner;
    private final JSpinner hjulSpinner;
    private final JSpinner passSpinner;

    private final JButton btnLagre;
    private final JButton btnOpprette;
    private final JButton btnSlette;
    private final JButton btnRedigere;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Oblig1Window frame = new Oblig1Window();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Object button;
    private JTextField AntPass;
    private JTextField AntHjul;

    /**
     * Create the frame.
     */
    public Oblig1Window() {

        this.loadFromDatabase();
        this.setTitle("Kjøretøyregister");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 571, 487);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{49, 0, 0};
        gbl_contentPane.rowHeights = new int[]{130, 16, 16, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_contentPane.columnWeights = new double[]{0.5, 0.5, 0.5};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);

        JLabel lblListeOverKjoretoy = new JLabel("Liste over kjøretøy:");
        GridBagConstraints gbc_lblListeOverKjoretoy = new GridBagConstraints();
        gbc_lblListeOverKjoretoy.anchor = GridBagConstraints.WEST;
        gbc_lblListeOverKjoretoy.insets = new Insets(0, 0, 5, 5);
        gbc_lblListeOverKjoretoy.gridx = 0;
        gbc_lblListeOverKjoretoy.gridy = 0;
        contentPane.add(lblListeOverKjoretoy, gbc_lblListeOverKjoretoy);

        DefaultListModel<Kjoretoy> kjoretoyModel = new DefaultListModel<>();
        for (Kjoretoy k : kjoretoyer) {
            kjoretoyModel.addElement(k);
        }

        kjoretoyListe = new JList<>();
        kjoretoyListe.addListSelectionListener(this);
        kjoretoyListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        kjoretoyListe.setModel(kjoretoyModel);

        JScrollPane scrollPane = new JScrollPane(kjoretoyListe);
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.gridwidth = 3;
        gbc_scrollPane.ipady = 200;
        gbc_scrollPane.ipadx = 300;
        gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
        gbc_scrollPane.gridx = 1;
        gbc_scrollPane.gridy = 0;
        contentPane.add(scrollPane, gbc_scrollPane);

        JLabel lblKjoretoyType = new JLabel("Kjøretøytype:");
        GridBagConstraints gbc_lblKjoretoyType = new GridBagConstraints();
        gbc_lblKjoretoyType.insets = new Insets(0, 0, 5, 5);
        gbc_lblKjoretoyType.anchor = GridBagConstraints.WEST;
        gbc_lblKjoretoyType.gridx = 0;
        gbc_lblKjoretoyType.gridy = 1;
        contentPane.add(lblKjoretoyType, gbc_lblKjoretoyType);

        kjoretoyType = new JComboBox<>();
        kjoretoyType.addItemListener(this);
        kjoretoyType.setEnabled(false);
        kjoretoyType.setModel(new DefaultComboBoxModel(new String[]{"", "TOHJULING", "FIRHJULING", "FLERHJULING"}));
        GridBagConstraints gbc_kjoretoyType = new GridBagConstraints();
        gbc_kjoretoyType.gridwidth = 3;
        gbc_kjoretoyType.insets = new Insets(0, 0, 5, 5);
        gbc_kjoretoyType.gridx = 1;
        gbc_kjoretoyType.gridy = 1;
        contentPane.add(kjoretoyType, gbc_kjoretoyType);

        JLabel lblRegnr = new JLabel("Registreringsnummer:");
        GridBagConstraints gbc_lblRegnr = new GridBagConstraints();
        gbc_lblRegnr.anchor = GridBagConstraints.WEST;
        gbc_lblRegnr.insets = new Insets(0, 0, 5, 5);
        gbc_lblRegnr.gridx = 0;
        gbc_lblRegnr.gridy = 2;
        contentPane.add(lblRegnr, gbc_lblRegnr);

        regNr = new JTextField();
        GridBagConstraints gbc_Regnr = new GridBagConstraints();
        gbc_Regnr.fill = GridBagConstraints.HORIZONTAL;
        gbc_Regnr.anchor = GridBagConstraints.WEST;
        gbc_Regnr.insets = new Insets(0, 0, 5, 0);
        gbc_Regnr.gridx = 1;
        gbc_Regnr.gridy = 2;
        contentPane.add(regNr, gbc_Regnr);
        regNr.setColumns(10);
        lblRegnr.setLabelFor(regNr);

        JLabel lblAnthjul = new JLabel("Antall hjul:");
        GridBagConstraints gbc_lblAnthjul = new GridBagConstraints();
        gbc_lblAnthjul.anchor = GridBagConstraints.WEST;
        gbc_lblAnthjul.insets = new Insets(0, 0, 5, 5);
        gbc_lblAnthjul.gridx = 2;
        gbc_lblAnthjul.gridy = 2;
        contentPane.add(lblAnthjul, gbc_lblAnthjul);

        hjulSpinner = new JSpinner();
        hjulSpinner.setEnabled(false);
        hjulSpinner.setModel(new SpinnerNumberModel(new Integer(2), new Integer(1), null, new Integer(1)));
        GridBagConstraints gbc_hjulSpinner = new GridBagConstraints();
        gbc_hjulSpinner.anchor = GridBagConstraints.WEST;
        gbc_hjulSpinner.insets = new Insets(0, 0, 5, 5);
        gbc_hjulSpinner.gridx = 3;
        gbc_hjulSpinner.gridy = 2;
        contentPane.add(hjulSpinner, gbc_hjulSpinner);

        JLabel lblAntpass = new JLabel("Antall passasjerer:");
        GridBagConstraints gbc_lblAntpass = new GridBagConstraints();
        gbc_lblAntpass.anchor = GridBagConstraints.WEST;
        gbc_lblAntpass.insets = new Insets(0, 0, 5, 5);
        gbc_lblAntpass.gridx = 2;
        gbc_lblAntpass.gridy = 3;
        contentPane.add(lblAntpass, gbc_lblAntpass);

        passSpinner = new JSpinner();
        passSpinner.setEnabled(false);
        passSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
        GridBagConstraints gbc_passSpinner = new GridBagConstraints();
        gbc_passSpinner.anchor = GridBagConstraints.WEST;
        gbc_passSpinner.insets = new Insets(0, 0, 5, 5);
        gbc_passSpinner.gridx = 3;
        gbc_passSpinner.gridy = 3;
        contentPane.add(passSpinner, gbc_passSpinner);

        JLabel lblMotorstr = new JLabel("Motorstørrelse:");
        GridBagConstraints gbc_lblMotorstr = new GridBagConstraints();
        gbc_lblMotorstr.anchor = GridBagConstraints.WEST;
        gbc_lblMotorstr.insets = new Insets(0, 0, 5, 5);
        gbc_lblMotorstr.gridx = 0;
        gbc_lblMotorstr.gridy = 4;
        contentPane.add(lblMotorstr, gbc_lblMotorstr);

        motorStr = new JTextField();
        GridBagConstraints gbc_Motorstr = new GridBagConstraints();
        gbc_Motorstr.insets = new Insets(0, 0, 5, 0);
        gbc_Motorstr.fill = GridBagConstraints.HORIZONTAL;
        gbc_Motorstr.gridx = 1;
        gbc_Motorstr.gridy = 4;
        contentPane.add(motorStr, gbc_Motorstr);
        motorStr.setColumns(10);

        lblAntDor = new JLabel("Antall dører:");
        GridBagConstraints gbc_lblAntDor = new GridBagConstraints();
        gbc_lblAntDor.anchor = GridBagConstraints.WEST;
        gbc_lblAntDor.insets = new Insets(0, 0, 5, 5);
        gbc_lblAntDor.gridx = 2;
        gbc_lblAntDor.gridy = 4;
        contentPane.add(lblAntDor, gbc_lblAntDor);

        dorSpinner = new JSpinner();
        dorSpinner.setEnabled(false);
        dorSpinner.setModel(new SpinnerNumberModel(1, 1, 4, 1));
        GridBagConstraints gbc_dorSpinner = new GridBagConstraints();
        gbc_dorSpinner.insets = new Insets(0, 0, 5, 5);
        gbc_dorSpinner.fill = GridBagConstraints.WEST;
        gbc_dorSpinner.gridx = 3;
        gbc_dorSpinner.gridy = 4;
        contentPane.add(dorSpinner, gbc_dorSpinner);

        JLabel lblHastighet = new JLabel("Hastighet:");
        GridBagConstraints gbc_lblHastighet = new GridBagConstraints();
        gbc_lblHastighet.anchor = GridBagConstraints.WEST;
        gbc_lblHastighet.insets = new Insets(0, 0, 5, 5);
        gbc_lblHastighet.gridx = 0;
        gbc_lblHastighet.gridy = 5;
        contentPane.add(lblHastighet, gbc_lblHastighet);

        hastighet = new JTextField();
        GridBagConstraints gbc_Hastighet = new GridBagConstraints();
        gbc_Hastighet.insets = new Insets(0, 0, 5, 0);
        gbc_Hastighet.fill = GridBagConstraints.HORIZONTAL;
        gbc_Hastighet.gridx = 1;
        gbc_Hastighet.gridy = 5;
        contentPane.add(hastighet, gbc_Hastighet);
        hastighet.setColumns(10);

        lblForbrukPrMil = new JLabel("Forbruk per mil:");
        GridBagConstraints gbc_lblForbrukPrMil = new GridBagConstraints();
        gbc_lblForbrukPrMil.anchor = GridBagConstraints.WEST;
        gbc_lblForbrukPrMil.insets = new Insets(0, 0, 5, 5);
        gbc_lblForbrukPrMil.gridx = 2;
        gbc_lblForbrukPrMil.gridy = 5;
        contentPane.add(lblForbrukPrMil, gbc_lblForbrukPrMil);

        forbrukPrMil = new JTextField();
        forbrukPrMil.setEnabled(false);
        GridBagConstraints gbc_forbrukPrMil = new GridBagConstraints();
        gbc_forbrukPrMil.insets = new Insets(0, 0, 5, 5);
        gbc_forbrukPrMil.fill = GridBagConstraints.HORIZONTAL;
        gbc_forbrukPrMil.gridx = 3;
        gbc_forbrukPrMil.gridy = 5;
        contentPane.add(forbrukPrMil, gbc_forbrukPrMil);
        forbrukPrMil.setColumns(4);

        JLabel lblTankvol = new JLabel("Tankvolum:");
        GridBagConstraints gbc_lblTankvol = new GridBagConstraints();
        gbc_lblTankvol.anchor = GridBagConstraints.WEST;
        gbc_lblTankvol.insets = new Insets(0, 0, 5, 5);
        gbc_lblTankvol.gridx = 0;
        gbc_lblTankvol.gridy = 6;
        contentPane.add(lblTankvol, gbc_lblTankvol);

        tankVol = new JTextField();
        GridBagConstraints gbc_Tankvol = new GridBagConstraints();
        gbc_Tankvol.insets = new Insets(0, 0, 5, 0);
        gbc_Tankvol.fill = GridBagConstraints.HORIZONTAL;
        gbc_Tankvol.gridx = 1;
        gbc_Tankvol.gridy = 6;
        contentPane.add(tankVol, gbc_Tankvol);
        tankVol.setColumns(10);

        lblDistansePrTank = new JLabel("Distanse pr tank:");
        GridBagConstraints gbc_lblDistansePrTank = new GridBagConstraints();
        gbc_lblDistansePrTank.anchor = GridBagConstraints.WEST;
        gbc_lblDistansePrTank.insets = new Insets(0, 0, 5, 5);
        gbc_lblDistansePrTank.gridx = 2;
        gbc_lblDistansePrTank.gridy = 6;
        contentPane.add(lblDistansePrTank, gbc_lblDistansePrTank);

        lblValueDistansePrTank = new JLabel("");
        GridBagConstraints gbc_lblValueDistansePrTank = new GridBagConstraints();
        gbc_lblValueDistansePrTank.insets = new Insets(0, 0, 5, 5);
        gbc_lblValueDistansePrTank.gridx = 3;
        gbc_lblValueDistansePrTank.gridy = 6;
        contentPane.add(lblValueDistansePrTank, gbc_lblValueDistansePrTank);

        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 5);
        gbc_panel.gridwidth = 4;
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 9;
        contentPane.add(panel, gbc_panel);

        btnOpprette = new JButton("Nytt kjøretøy");
        btnOpprette.addActionListener(this);
        btnOpprette.setEnabled(true);
        panel.add(btnOpprette);

        btnLagre = new JButton("Lagre");
        btnLagre.addActionListener(this);

        btnSlette = new JButton("Slette");
        btnSlette.addActionListener(this);
        btnSlette.setEnabled(false);
        panel.add(btnSlette);

        btnRedigere = new JButton("Redigere");
        btnRedigere.addActionListener(this);
        btnRedigere.setEnabled(false);
        panel.add(btnRedigere);
        btnLagre.setEnabled(false);
        panel.add(btnLagre);

        errorLabel = new JLabel("");
        errorLabel.setForeground(Color.RED);
        GridBagConstraints gbc_errorLabel = new GridBagConstraints();
        gbc_errorLabel.anchor = GridBagConstraints.PAGE_END;
        gbc_errorLabel.gridwidth = 3;
        gbc_errorLabel.insets = new Insets(0, 0, 0, 5);
        gbc_errorLabel.gridx = 0;
        gbc_errorLabel.gridy = 8;
        contentPane.add(errorLabel, gbc_errorLabel);

    }


    public void loadFromDatabase() {
        kjoretoyer = this.databaseManager.loadFromDatabase();
    }


    public void loadFile() {
        String filNavn = "kjoretoyListe.data";
        try {
            kjoretoyer = new ArrayList<>();
            // les inn objekter fra fil vha en fileinputstream
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filNavn));
            Object obj = null;
            while ((obj = ois.readObject()) != null) {
                if (obj instanceof Kjoretoy) { // Sjekk at innlest objekt er av korrekt type
                    kjoretoyer.add((Kjoretoy) obj);
                }
            }
            ois.close();
        } catch (ClassNotFoundException noClass) {
            System.out.println(noClass.getMessage());
        } catch (FileNotFoundException noFileException) {
            // Fil finnes ikke, skriv ut feilmelding og fortsett
            System.out.println(noFileException.getMessage());
        } catch (IOException inputException) {
            // Denne oppstår ved ikke flere objekter i fila
        }
    }

    public void saveToFile() {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(kjoretoyFil));
            for (Kjoretoy k : this.getkjoretoyer()) {
                oos.writeObject(k);
            }
            oos.close();
        } catch (IOException outputException) {
            System.out.println(outputException.getMessage());
        } finally {
            System.out.println("Lagret kjøretøyregisteret til fil: " + kjoretoyFil);
            System.out.println("Kjøretøyregisteret inneholdt " + this.getSize() + " kjøretøy.");
        }
    }

    public void saveToDatabase() {
        this.databaseManager.saveToDatabase(kjoretoyer);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {
            if (this.kjoretoyListe.getSelectedIndex() == -1) {
                //No selection, disable button.
                this.resetForm(false, true);
                this.getBtnOpprette().setEnabled(true);
                this.getBtnSlette().setEnabled(false);
                this.getBtnLagre().setEnabled(false);
                this.getBtnRedigere().setEnabled(false);
            } else {
                //Selection, enable the button
                this.getBtnRedigere().setEnabled(true);
                this.getBtnOpprette().setEnabled(false);
                this.getBtnSlette().setEnabled(true);
                Kjoretoy k = this.kjoretoyListe.getSelectedValue();
                this.getRegNr().setText(k.getRegNr());
                this.getMotorStr().setText(Integer.toString(k.getMotorStr()));
                this.getHjulSpinner().setValue(k.getAntHjul());
                this.getPassSpinner().setValue(k.getAntPass());
                this.getTankVol().setText(Integer.toString(k.getTankVol()));
                this.getForbrukPrMil().setText(Double.toString(k.getforbrukPrMil()));
                try {
                    this.getlblValueDistansePrTank().setText(Double.toString(k.getDistansePrTank()));
                } catch (Exception e1) {
                    this.getErrorLabel().setText(e1.getMessage());
                }
                this.getHastighet().setText(Integer.toString(k.getHastighet()));
                if (k instanceof KjoretoyMedDorer) {
                    this.getlblAntDor().setVisible(true);
                    this.getDorSpinner().setVisible(true);
                    this.getDorSpinner().setValue(((KjoretoyMedDorer) k).getAntDor());
                } else {
                    this.getlblAntDor().setVisible(false);
                    this.getDorSpinner().setVisible(false);
                }
            }
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
        switch (e.getActionCommand()) {
            case "Nytt kjøretøy":
                this.resetForm(true, true);
                this.getBtnLagre().setEnabled(true);
                this.getBtnOpprette().setEnabled(false);
                break;
            case "Lagre":
                try {
                    if (this.kjoretoyType.getSelectedIndex() == 0) {
                        this.getErrorLabel().setText("Mangler kjøretøytype");
                        break;
                    }

                    if (this.getRegNr().getText().equals("")) {
                        this.getErrorLabel().setText("Mangler registreringsnummer");
                        break;
                    }


                    if (this.motorStr.getText().equals("")) {
                        this.getErrorLabel().setText("Mangler motorstørrelse");
                        break;
                    }

                    if (this.hastighet.getText().equals("")) {
                        this.getErrorLabel().setText("Mangler hastighet");
                        break;
                    }

                    if (this.tankVol.getText().equals("")) {
                        this.getErrorLabel().setText("Mangler tankvolum");
                        break;
                    }

                    if (Integer.parseInt(this.hjulSpinner.getValue().toString()) == 0) {
                        this.getErrorLabel().setText("Mangler antall hjul");
                        break;
                    }

                    if (Integer.parseInt(this.passSpinner.getValue().toString()) == 0) {
                        this.getErrorLabel().setText("Mangler antall passasjerer");
                        break;
                    }

                    if (this.forbrukPrMil.getText().equals("")) {
                        this.getErrorLabel().setText("Mangler forbruk per mil");
                        break;
                    }

                    if (Integer.parseInt(this.dorSpinner.getValue().toString()) == 0) {
                        this.getErrorLabel().setText("Mangler antall dører");
                        break;
                    }

                    if (!this.getRegNr().getText().equals("")) {
                        this.getErrorLabel().setText("");
                        String RegNr = this.getRegNr().getText();
                        String kjoretoyType = this.kjoretoyType.getSelectedItem().toString();
                        int AntHjul = (int) this.getHjulSpinner().getValue();
                        int AntDor = (int) this.getDorSpinner().getValue();
                        int motorStr = Integer.parseInt(this.getMotorStr().getText());
                        int tankVol = Integer.parseInt(this.getTankVol().getText());
                        int antPass = (int) this.getPassSpinner().getValue();
                        double forbrukPrMil = Double.parseDouble(this.getForbrukPrMil().getText());
                        int Hastighet = Integer.parseInt(this.getHastighet().getText());
                        Kjoretoy nyttKjoretoy = null;
                        boolean kjoretoyErNytt = true;
                        if (this.kjoretoyListe.getSelectedValue() != null) {
                            nyttKjoretoy = this.kjoretoyListe.getSelectedValue();
                            kjoretoyErNytt = false;
                        } else {
                            if (kjoretoyType == "TOHJULING") {
                                nyttKjoretoy = new Kjoretoy(kjoretoyType, AntHjul, RegNr, motorStr, antPass, Hastighet, tankVol, forbrukPrMil);

                            } else {
                                System.out.println("KJØRETØY MED DØRER!!");
                                nyttKjoretoy = new KjoretoyMedDorer(AntDor, kjoretoyType, AntHjul, RegNr, motorStr, antPass, Hastighet, tankVol, forbrukPrMil);
                            }

                            this.databaseManager.insertKjoretoy(nyttKjoretoy);
                        }
                        nyttKjoretoy.setRegNr(RegNr);
                        nyttKjoretoy.setMotorStr(motorStr);
                        nyttKjoretoy.setTankVol(tankVol);
                        nyttKjoretoy.setAntPass(antPass);
                        nyttKjoretoy.setforbrukPrMil(forbrukPrMil);
                        nyttKjoretoy.setHastighet(Hastighet);
                        if (kjoretoyErNytt) {
                            kjoretoyer.add(nyttKjoretoy);
                        }
                        DefaultListModel<Kjoretoy> temp = new DefaultListModel<>();
                        for (Kjoretoy k : kjoretoyer) {
                            temp.addElement(k);
                        }
                        this.kjoretoyListe.setModel(temp);
                        this.resetForm(false, true);
                        this.getBtnLagre().setEnabled(false);
                        this.getBtnOpprette().setEnabled(true);
                    } else {
                        this.getErrorLabel().setText("Kan ikke lagre kjøretøy uten registreringsnummer.");
                    }
                } catch (NumberFormatException ex) {
                    this.getErrorLabel().setText("Det er feil i input.");
                }
                break;
            case "Slette":
                this.databaseManager.deleteKjoretoy(this.kjoretoyListe.getSelectedValue());
                this.kjoretoyer.remove(this.kjoretoyListe.getSelectedIndex());
                DefaultListModel<Kjoretoy> temp = new DefaultListModel<>();
                for (Kjoretoy k : kjoretoyer) {
                    temp.addElement(k);
                }
                this.kjoretoyListe.setModel(temp);
                this.resetForm(false, true);
                break;
            case "Redigere":
                this.resetForm(true, false);
                this.getBtnRedigere().setEnabled(false);
                this.getBtnLagre().setEnabled(true);
                this.getBtnSlette().setEnabled(false);
                break;

            default:
                System.out.println("Ikke implementert knapp");
        }
    }

    private void resetForm(Boolean formState, Boolean emptyForm) {
        if (emptyForm) {
            //this.kjoretoyListe.removeSelectionInterval(0, 0);

            this.kjoretoyType.setSelectedIndex(0);
            this.getRegNr().setText("");
            this.getMotorStr().setText("");
            this.getHjulSpinner().setValue(2);
            this.getPassSpinner().setValue(1);
            this.getTankVol().setText("");
            this.getForbrukPrMil().setText("");
            this.getHastighet().setText("");
            //this.getDorSpinner().setValue("");
        }
        this.kjoretoyType.setEnabled(formState);
        this.getRegNr().setEnabled(formState);
        if (this.getRegNr().getText().equals("")) {
            this.getRegNr().setEnabled(formState);
        }
        this.getMotorStr().setEnabled(formState);
        this.getHjulSpinner().setEnabled(formState);
        this.getPassSpinner().setEnabled(formState);
        this.getTankVol().setEnabled(formState);
        this.getForbrukPrMil().setEnabled(formState);
        this.getHastighet().setEnabled(formState);
        this.getDorSpinner().setEnabled(formState);
        this.getKjoretoyListe().setEnabled(!formState);
    }

    /*
    private Kjoretoy nyttKjoretoy(int type) {
		Kjoretoy nyttKjoretoy = null;
		int AntDor = 0;
		
		switch (type) {
			case Kjoretoy.TOHJULING:
                            nyttKjoretoy = new Kjoretoy(kjoretoyType, this.getRegNr().getText(), AntHjul);
                            getInput(nyttKjoretoy, kjoretoyType, AntHjul);
			case Kjoretoy.FIRHJULING:
			case Kjoretoy.FLERHJULING:
                            AntDor = parseInput();
			break;
			default:
				System.out.println("Ukjent kjøretøytype");
		}
		return nyttKjoretoy;
	}	
*/
    @Override
    public void itemStateChanged(ItemEvent e) {
        this.getErrorLabel().setText("");
        if (e.getStateChange() == ItemEvent.SELECTED) {
            if (this.kjoretoyType.getSelectedItem() == "TOHJULING") {
                this.getDorSpinner().setVisible(false);
                this.getlblAntDor().setVisible(false);
            } else {
                this.getDorSpinner().setVisible(true);
                this.getlblAntDor().setVisible(true);
            }
            if (this.kjoretoyType.getSelectedIndex() == 0) {
                this.getErrorLabel().setText("Kjøretøytype må være valgt.");
            }
        }
    }

    public JLabel getErrorLabel() {
        return errorLabel;
    }

    public JButton getBtnOpprette() {
        return btnOpprette;
    }

    public JButton getBtnLagre() {
        return btnLagre;
    }

    public JButton getBtnSlette() {
        return btnSlette;
    }

    public JTextField getRegNr() {
        return regNr;
    }

    public JTextField getMotorStr() {
        return motorStr;
    }

    public JTextField getTankVol() {
        return tankVol;
    }

    public JTextField getHastighet() {
        return hastighet;
    }

    public JList<Kjoretoy> getKjoretoyListe() {
        return kjoretoyListe;
    }

    public JComboBox<String> getKjoretoyType() {
        return kjoretoyType;
    }

    public JLabel getlblAntDor() {
        return lblAntDor;
    }

    public JTextField getForbrukPrMil() {
        return forbrukPrMil;
    }


    public JLabel getlblValueDistansePrTank() {
        return lblValueDistansePrTank;
    }

    public JSpinner getDorSpinner() {
        return dorSpinner;
    }

    public JSpinner getHjulSpinner() {
        return hjulSpinner;
    }

    public JSpinner getPassSpinner() {
        return passSpinner;
    }

    public JButton getBtnRedigere() {
        return btnRedigere;
    }
}
