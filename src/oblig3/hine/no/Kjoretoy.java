
package oblig3.hine.no;

import java.io.Serializable;


public class Kjoretoy implements Serializable {
    private static final long serialVersionUID = 1L;
    public String kjoretoyType;
    private int antHjul;
    private String regNr;
    private int motorStr;
    private int antPass;
    private int hastighet;
    private int tankVol;
    private double forbrukPrMil;


    public Kjoretoy(String kjoretoyType, int antHjul, String regNr, int motorStr, int antPass, int hastighet, int tankVol, double forbrukPrMil) {
        this.kjoretoyType = kjoretoyType;
        this.antHjul = antHjul;
        this.regNr = regNr;
        this.motorStr = motorStr;
        this.antPass = antPass;
        this.hastighet = hastighet;
        this.tankVol = tankVol;
        this.forbrukPrMil = forbrukPrMil;
    }

    public int getAntHjul() {
        return antHjul;
    }

    public void setAntHjul(int antHjul) {
        this.antHjul = antHjul;
    }

    public String getRegNr() {
        return regNr;
    }

    public void setRegNr(String regNr) {
        this.regNr = regNr;
    }

    public int getAntPass() {
        return antPass;
    }

    public void setAntPass(int antPass) {
        this.antPass = antPass;
    }

    public int getMotorStr() {
        return motorStr;
    }

    public void setMotorStr(int motorStr) {
        this.motorStr = motorStr;
    }

    public int getHastighet() {
        return hastighet;
    }

    public void setHastighet(int hastighet) {
        this.hastighet = hastighet;
    }

    public int getTankVol() {
        return tankVol;
    }

    public void setTankVol(int tankVol) {
        this.tankVol = tankVol;
    }

    public String getkjoretoyType() {
        return kjoretoyType;
    }

    public double getforbrukPrMil() {
        return forbrukPrMil;
    }

    public void setforbrukPrMil(double forbrukPrMil) {
        this.forbrukPrMil = forbrukPrMil;
    }

    public double getDistansePrTank() {
        if (this.getforbrukPrMil() != 0) {
            return this.getTankVol() / this.getforbrukPrMil();
        } else {
            return 0;
        }
    }


    @Override
    public String toString() {
        return this.getkjoretoyType() +
                ", regnr: " + this.getRegNr() +
                ", antall hjul: " + this.getAntHjul() +
                ", antall passasjerer: " + this.getAntPass() +
                ", motorstørrelse: " + this.getMotorStr() +
                ", hastighet: " + this.getHastighet() +
                ", tankvolum: " + this.getTankVol() +
                ", forbruk pr mil " + this.getforbrukPrMil() +
                ", distanse pr tank " + this.getDistansePrTank();

    }
}