/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oblig3.hine.no;

/**
 * @author SiljeAndrea
 */
public class KjoretoyMedDorer extends Kjoretoy {
    int antDor;

    public KjoretoyMedDorer(int antDor, String kjoretoyType, int antHjul, String regNr, int motorStr, int antPass, int hastighet, int tankVol, double forbrukPrMil) {
        super(kjoretoyType, antHjul, regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil);
        this.antDor = antDor;
    }

    @Override
    public String toString() {
        return super.toString() + ", antDor=" + antDor;
    }


    public int getAntDor() {
        return antDor;
    }

    public void setAntDor(int antDor) {
        this.antDor = antDor;
    }


}
