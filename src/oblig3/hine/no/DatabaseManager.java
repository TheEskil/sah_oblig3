package oblig3.hine.no;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DatabaseManager {
    private Connection dbConnection = null;

    private final static String dbUrl = "jdbc:derby://localhost:1527/vehicle_data_siljeah;create=true";


    private final String createKjoretoyTableSql = "CREATE TABLE kjoretoyListe("
            + "id INT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
            + "antDor INT,"
            + "kjoretoyType VARCHAR(20) NOT NULL,"
            + "antHjul INT NOT NULL,"
            + "regNr VARCHAR(20) NOT NULL,"
            + "motorStr INT NOT NULL,"
            + "antPass INT NOT NULL,"
            + "hastighet INT NOT NULL,"
            + "tankVol INT NOT NULL,"
            + "forbrukPrMil DOUBLE NOT NULL,"
            + "CONSTRAINT primary_key PRIMARY KEY (id))";

    private final String allKjoretoySql = "SELECT * FROM kjoretoyListe";

    private final String insertKjoretoySqlMDor = "INSERT INTO kjoretoyListe(antDor,kjoretoyType,antHjul,regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil) VALUES (?,?,?,?,?,?,?,?,?)";
    private final String insertKjoretoySqlUDor = "INSERT INTO kjoretoyListe(kjoretoyType,antHjul,regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil) VALUES (?,?,?,?,?,?,?,?)";


    private final String updateKjoretoySql = "UPDATE kjoretoyListe SET antDor = ?, kjoretoyType = ?, antHjul = ?, regNr = ?, motorStr = ?, antPass = ?, hastighet = ?, tankVol = ?, forbrukPrMil = ? WHERE id = ?";

    private final String deleteKjoretoySql = "DELETE FROM kjoretoyListe WHERE regNr = ?";


    public DatabaseManager() {
        createConnection();
        //deleteDbTable(); // Kun for debug
        setupDbTable();
        //truncateDbTable(); // Kun for debug
    }

    private void createConnection() {
        try {
            this.dbConnection = DriverManager.getConnection(dbUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupDbTable() {
        try {
            Statement statement = this.dbConnection.createStatement();
            DatabaseMetaData databaseMetaData = this.dbConnection.getMetaData();

            ResultSet resultSet = databaseMetaData.getTables(null, "APP", "KJORETOYLISTE", null);

            if (!resultSet.next()) {
                if (!statement.execute(this.createKjoretoyTableSql)) {
                    System.out.println("Opprettet tabellen 'kjoretoyListe'");
                }
            } else {
                System.out.println("Tabellen 'kjoretoyListe' finnes allerede.");
                resultSet.close();
            }

            statement.close();
        } catch (SQLException e) {
            System.out.println("SQL er litt off!!!!!");
            e.printStackTrace();
        }
    }

    // Kun for debug
    public void deleteDbTable() {
        try {
            Statement dropStatement = this.dbConnection.createStatement();

            if (!dropStatement.execute("DROP TABLE kjoretoyListe")) {
                System.out.println("Slettet databasen.");
            }

            dropStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Kun for debug
    public void truncateDbTable() {
        try {
            Statement truncateStatement = this.dbConnection.createStatement();

            if (!truncateStatement.execute("DELETE FROM kjoretoyListe")) {
                System.out.println("Tømte databasen.");
            }

            truncateStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
/*
    public void insertKjoretoy(Kjoretoy kjoretoy) {
		try {
			PreparedStatement insertKjoretoyPrep = this.dbConnection.prepareStatement(this.insertKjoretoySql);
                        
			insertKjoretoyPrep.setString(2, kjoretoy.getkjoretoyType());
                        insertKjoretoyPrep.setInt(3, kjoretoy.getAntHjul());
                        insertKjoretoyPrep.setString(4, kjoretoy.getRegNr());
			insertKjoretoyPrep.setInt(5, kjoretoy.getMotorStr());
			insertKjoretoyPrep.setInt(6, kjoretoy.getAntPass());
			insertKjoretoyPrep.setInt(7, kjoretoy.getHastighet());
			insertKjoretoyPrep.setInt(8, kjoretoy.getTankVol());
			insertKjoretoyPrep.setDouble(9, kjoretoy.getforbrukPrMil());
                        
			if(!(kjoretoy instanceof Tohjuling)) {
				KjoretoyMedDorer kjoretoyMedDorer = (KjoretoyMedDorer) kjoretoy;

				insertKjoretoyPrep.setInt(1, kjoretoyMedDorer.getAntDor());
			} else {
				insertKjoretoyPrep.setInt(9, 0);
			}
                    
			insertKjoretoyPrep.execute();
			insertKjoretoyPrep.close();

			System.out.println("Lagt til i databasen: " + kjoretoy);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
*/

    public void insertKjoretoy(Kjoretoy kjoretoy) {
        try {
            PreparedStatement insertKjoretoyPrepMDor = this.dbConnection.prepareStatement(this.insertKjoretoySqlMDor);
            PreparedStatement insertKjoretoyPrepUDor = this.dbConnection.prepareStatement(this.insertKjoretoySqlUDor);
            if (kjoretoy instanceof KjoretoyMedDorer) {
                insertKjoretoyPrepMDor.setString(2, kjoretoy.getkjoretoyType());
                insertKjoretoyPrepMDor.setInt(3, kjoretoy.getAntHjul());
                insertKjoretoyPrepMDor.setString(4, kjoretoy.getRegNr());
                insertKjoretoyPrepMDor.setInt(5, kjoretoy.getMotorStr());
                insertKjoretoyPrepMDor.setInt(6, kjoretoy.getAntPass());
                insertKjoretoyPrepMDor.setInt(7, kjoretoy.getHastighet());
                insertKjoretoyPrepMDor.setInt(8, kjoretoy.getTankVol());
                insertKjoretoyPrepMDor.setDouble(9, kjoretoy.getforbrukPrMil());
                KjoretoyMedDorer kjoretoyMedDorer = (KjoretoyMedDorer) kjoretoy;
                insertKjoretoyPrepMDor.setInt(1, kjoretoyMedDorer.getAntDor());
                insertKjoretoyPrepMDor.execute();
                insertKjoretoyPrepMDor.close();
            } else {
                insertKjoretoyPrepUDor.setString(1, kjoretoy.getkjoretoyType());
                insertKjoretoyPrepUDor.setInt(2, kjoretoy.getAntHjul());
                insertKjoretoyPrepUDor.setString(3, kjoretoy.getRegNr());
                insertKjoretoyPrepUDor.setInt(4, kjoretoy.getMotorStr());
                insertKjoretoyPrepUDor.setInt(5, kjoretoy.getAntPass());
                insertKjoretoyPrepUDor.setInt(6, kjoretoy.getHastighet());
                insertKjoretoyPrepUDor.setInt(7, kjoretoy.getTankVol());
                insertKjoretoyPrepUDor.setDouble(8, kjoretoy.getforbrukPrMil());
                insertKjoretoyPrepUDor.execute();
                insertKjoretoyPrepUDor.close();
            }


            System.out.println("Lagt til i databasen: " + kjoretoy);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void updateKjoretoy(Kjoretoy kjoretoy) {
        int vehicleId = -1;

        try {
            PreparedStatement getKjoretoyPrep = this.dbConnection.prepareStatement("SELECT * FROM kjoretoyListe WHERE regNr = ?");

            getKjoretoyPrep.setString(1, kjoretoy.getRegNr());

            ResultSet resultSet = getKjoretoyPrep.executeQuery();

            if (resultSet.next()) {
                vehicleId = resultSet.getInt("id");
            } else {
                return;
            }

            resultSet.close();
            getKjoretoyPrep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement updateKjoretoyPrep = this.dbConnection.prepareStatement(this.updateKjoretoySql);
            //antDor,kjoretoyType,antHjul,regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil
            if (kjoretoy instanceof KjoretoyMedDorer) {
                updateKjoretoyPrep.setString(2, kjoretoy.getkjoretoyType());
                updateKjoretoyPrep.setInt(3, kjoretoy.getAntHjul());
                updateKjoretoyPrep.setString(4, kjoretoy.getRegNr());
                updateKjoretoyPrep.setInt(5, kjoretoy.getMotorStr());
                updateKjoretoyPrep.setInt(6, kjoretoy.getAntPass());
                updateKjoretoyPrep.setInt(7, kjoretoy.getHastighet());
                updateKjoretoyPrep.setInt(8, kjoretoy.getTankVol());
                updateKjoretoyPrep.setDouble(9, kjoretoy.getforbrukPrMil());
                KjoretoyMedDorer kjoretoyMedDorer = (KjoretoyMedDorer) kjoretoy;
                updateKjoretoyPrep.setInt(1, kjoretoyMedDorer.getAntDor());
            } else {
                updateKjoretoyPrep.setString(2, kjoretoy.getkjoretoyType());
                updateKjoretoyPrep.setInt(3, kjoretoy.getAntHjul());
                updateKjoretoyPrep.setString(4, kjoretoy.getRegNr());
                updateKjoretoyPrep.setInt(5, kjoretoy.getMotorStr());
                updateKjoretoyPrep.setInt(6, kjoretoy.getAntPass());
                updateKjoretoyPrep.setInt(7, kjoretoy.getHastighet());
                updateKjoretoyPrep.setInt(8, kjoretoy.getTankVol());
                updateKjoretoyPrep.setDouble(9, kjoretoy.getforbrukPrMil());
                updateKjoretoyPrep.setInt(1, 0);
            }

            updateKjoretoyPrep.setDouble(10, vehicleId);
            updateKjoretoyPrep.execute();
            updateKjoretoyPrep.close();

            System.out.println("Oppdatert i databasen: " + kjoretoy);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


  /*
        public void updateKjoretoy(Kjoretoy kjoretoy) {
		int vehicleId = -1;

		try {
			PreparedStatement getKjoretoyPrep = this.dbConnection.prepareStatement("SELECT * FROM kjoretoyListe WHERE regNr = ?");

			getKjoretoyPrep.setString(1, kjoretoy.getRegNr());

			ResultSet resultSet = getKjoretoyPrep.executeQuery();

			if(resultSet.next()) {
				vehicleId = resultSet.getInt("id");
			}
			else {
				return;
			}

			resultSet.close();
			getKjoretoyPrep.close();
		} catch(SQLException e) {
			e.printStackTrace();
		}

		try {
			PreparedStatement updateKjoretoyPrep = this.dbConnection.prepareStatement(this.updateKjoretoySql);
			//antDor,kjoretoyType,antHjul,regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil
                        updateKjoretoyPrep.setString(2, kjoretoy.getkjoretoyType());
			updateKjoretoyPrep.setInt(3, kjoretoy.getAntHjul());
                        updateKjoretoyPrep.setString(4, kjoretoy.getRegNr());
			updateKjoretoyPrep.setInt(5, kjoretoy.getMotorStr());
			updateKjoretoyPrep.setInt(6, kjoretoy.getAntPass());
			updateKjoretoyPrep.setInt(7, kjoretoy.getHastighet());
			updateKjoretoyPrep.setInt(8, kjoretoy.getTankVol());

			if(!(kjoretoy instanceof Tohjuling)) {
				KjoretoyMedDorer kjoretoyMedDorer = (KjoretoyMedDorer) kjoretoy;

				updateKjoretoyPrep.setInt(1, kjoretoyMedDorer.getAntDor());
			} else {
                                updateKjoretoyPrep.setInt(1, 0);

			}
			updateKjoretoyPrep.setDouble(9, kjoretoy.getforbrukPrMil());
			updateKjoretoyPrep.execute();
			updateKjoretoyPrep.close();

			System.out.println("Oppdatert i databasen: " + kjoretoy);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
*/


    public void deleteKjoretoy(Kjoretoy kjoretoy) {
        try {
            PreparedStatement deleteKjoretoyPrep = this.dbConnection.prepareStatement(this.deleteKjoretoySql);

            deleteKjoretoyPrep.setString(1, kjoretoy.getRegNr());
            if (!deleteKjoretoyPrep.execute()) {
                System.out.println("Slettet i databasen: " + kjoretoy);
            }

            deleteKjoretoyPrep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Kjoretoy> loadFromDatabase() {
        List<Kjoretoy> listeOverKjoretoy = new ArrayList<>();

        try {
            Statement statement = this.dbConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(this.allKjoretoySql);

            Kjoretoy kjoretoy = null;
            while (resultSet.next()) {
                switch (resultSet.getString("kjoretoyType")) {
                    case "FLERHJULING":
                    case "FIRHJULING":
                        //antDor,kjoretoyType,antHjul,regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil
                        kjoretoy = new KjoretoyMedDorer(resultSet.getInt("antDor"), resultSet.getString("kjoretoyType"), resultSet.getInt("antHjul"), resultSet.getString("regNr"), resultSet.getInt("motorStr"), resultSet.getInt("antPass"), resultSet.getInt("hastighet"), resultSet.getInt("tankVol"), resultSet.getDouble("forbrukPrMil"));
                        break;
                    case "TOHJULING":
                        kjoretoy = new Kjoretoy(resultSet.getString("kjoretoyType"), resultSet.getInt("antHjul"), resultSet.getString("regNr"), resultSet.getInt("motorStr"), resultSet.getInt("antPass"), resultSet.getInt("hastighet"), resultSet.getInt("tankVol"), resultSet.getDouble("forbrukPrMil"));
                        break;

                }
                listeOverKjoretoy.add(kjoretoy);

                System.out.println("Lastet inn fra databasen: " + kjoretoy);
            }

            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listeOverKjoretoy;
    }

    public void saveToDatabase(List<Kjoretoy> listeOverKjoretoy) {
        try {
            PreparedStatement getKjoretoyPrep = this.dbConnection.prepareStatement("SELECT * FROM kjoretoyListe WHERE regNr = ?");

            for (Kjoretoy kjoretoy : listeOverKjoretoy) {
                getKjoretoyPrep.setString(1, kjoretoy.getRegNr());
                ResultSet resultSet = getKjoretoyPrep.executeQuery();

                if (resultSet.next()) {
                    this.updateKjoretoy(kjoretoy);
                } else {
                    System.out.println("LAGRER");
                    this.insertKjoretoy(kjoretoy);
                }

                resultSet.close();
            }

            getKjoretoyPrep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
