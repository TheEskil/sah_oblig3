
package oblig3.hine.no;

/**
 * @author SiljeAndrea
 */
public class Tohjuling extends Kjoretoy {


    public Tohjuling(String kjoretoyType, int antHjul, String regNr, int motorStr, int antPass, int hastighet, int tankVol, double forbrukPrMil) {
        super(kjoretoyType, antHjul, regNr, motorStr, antPass, hastighet, tankVol, forbrukPrMil);

    }


    @Override
    public String toString() {
        return "Tohjuling{" + super.toString() + '}';
    }


}
